export default function() {
    
    //array
    const items = [1, 2, 3, 4];

    //expression bodies or lambda
    let byTwo = items.map(i => i * 2);

    //with a block
    let byFour = items.map(i => {
        return i * 4;
    });
    console.log(byTwo);
    console.log(byFour);

    //lexical this
    function Person() {
        this.name = "Charles";
        this.nickNames = ["Charlie", "Chuck", "Chuckles"];
        this.print = () => {
            return this.nickNames.map((n) => {
                return this.name + " is nicknamed " + n;
            });
        };
    }

    console.log(new Person().print());
    //["Charles is nicknamed Charlie","Charles is nicknamed Chuck",
    //"Charles is nicknamed Chuckles"]
};