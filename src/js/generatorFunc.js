export default function() {
    //generator
    function* idMaker(idx) {
        var index = idx || 0;

        //infinite loop!
        while (true) {

            //get the value of next, and yield next index
            var reset = yield index++;

            //did they ask for a reset?
            if (reset) {
                index = idx || 0;
            }

        }
    }

    //create new generator iterator, notice no 'new'
    const gen = idMaker();

    //grab some IDs
    //each next returns > { value: val, done: false }
    const ids = [gen.next(), gen.next(), gen.next(true)];

    console.log(ids); 
}