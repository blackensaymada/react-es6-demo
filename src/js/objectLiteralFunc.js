export default function() {

    let name = "Charlie", age = 29;

    const me = {
        name,
        age,
        //old way
        weight: function(){
            return 86.1826;
        },
        //new hotness
        height() {
            return 180;
        }
    }

    //view the results
    console.log(me.name);
    console.log(me.age);
    console.log(me.weight());
    console.log(me.height());
};