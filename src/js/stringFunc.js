export default function() {

    //define some classes
    const style = {
        width: "25px",
        height: "50px",
        display: "inline-block"
    }
    //arbitrary padding function
    function getPadding() {
        return 44;
    }

    //interpolate a literal string
    let tmpl = `
    <div style="display:${style.display};padding:${getPadding()};">
        <span style="width:${style.width};height:${style.height}">
            Hi it's ${new Date()}
        </span>
     </div >
    `;

    //log our string literal
    console.log(tmpl);

}