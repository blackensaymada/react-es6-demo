export default function() {
    //works on arrays, or objects
    function getName(args) {

        if ( Array.isArray(args) ) {

            //assign the first and second element of the array
            let [firstName,lastName] = args;
            return `${firstName} ${lastName}`;

        } else {

            //assign the firstName key and lastName key
            let {firstName,lastName} = args;
            return `${firstName} ${lastName}`;

        }

    }

    console.log(getName(["Charlie", "King"]));

    //you can skip elements
    let items = [ 1,2,3,4,5 ];

    //new hotness
    let [,second, third, fourth] = items;

    console.log([second,third,fourth]);

    //useful for selecting keys inside an expression
    let books = [
        { name:"Javascript: The Good Parts", ISBN:1224283},
        { name: "Effective Javascript", ISBN:128263},
        { name: "The Principles of Object-Oriented JavaScript", ISBN:122663}
    ];

    //old way
    let bookISBNS = books.map(function(b){
        return b.ISBN;
    });
    //new hotness
    let bookNames = books.map( ({name}) => name);

    console.log(bookISBNS);
    console.log(bookNames);
                
};