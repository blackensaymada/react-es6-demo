export default function() {
    const apiBase = "https://mysite.com/api/v1/";
    const clientId = "Adxk38dn3c9u1ndk";

    //block scoped
    if (true) {

        const apiBase = "https://othersite.com/api/v2/"; 

        console.log(apiBase + clientId); 
        //https://othersite.com/api/v2
    }

    apiBase = "https://somewhereelse";
    //Syntax error - apiBase is read-only

    console.log(apiBase+clientId); 
    //https://mysite.com/api/v1/Adxk38dn3c9u1ndk
};