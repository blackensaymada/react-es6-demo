export function mapFunc() {
    var m = new Map();
    m.set("hello", 42);
                    
    //create an obj
    const Person = {
        name: "Charlie"
    }
    //add the complex key
    m.set(Person, "present");

    //get the value
    m.get(Person) === "present";

    //iterable
    for (var v of m.values()) {
        console.log("value:" +v);
    }

    for (var k of m.keys()) {
        console.log("key:" + JSON.stringify(k));
    }

    //supports size, not length;
    console.log(m.size);
};

export function setFunc() {

    var s = new Set();
    s.add("hello");

    //create an obj
    const Person = {
        name: "Charlie"
    }

    //add the complex value
    s.add(Person);

    //get the value
    s.has(Person) === true;

    // try re-add the same value, ignored
    s.add(Person);

    //iterable
    for (var item of s) {
        console.log(item);
    }

    //supports size;
    console.log(s.size);

};

export function weakMap() {
    //Weak, ya super weak.
    var wm = new WeakMap();

    //create an obj
    const Person = {
        name: "Charlie"
    }

    wm.set(Person, "present");

    //get the value
    console.log(wm.has(Person) === true);
};

export function weakSet() {
    //weak set, bro
    var ws = new WeakSet();

    //create an obj
    const Person = {
        name: "Charlie"
    }

    ws.add(Person);

    //get the value
    console.log(ws.has(Person) === true);
}