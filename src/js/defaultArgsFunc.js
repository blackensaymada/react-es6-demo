export default function() {
    //OMG default params
    function getApiUrl (resource, base = "http://b.com/api") {
        return `${base}/${resource}`;
    }

    let staticBase = "http://b.com/content";

    //you can utilize outer scope
    function getStaticUrl (resource, base = staticBase) {
        return `${base}/${resource}`;
    }

    ////OMGWTF expressions in params!! 
    function getThumb(resource, width, dpi = width/10 ) {
        return `${staticBase}/${resource}?width=${width}&dpi=${dpi}`;
    }

    console.log(getApiUrl("image"));
    console.log(getStaticUrl("doc.pdf"));
    console.log(getThumb("image.jpg",200));
};