export default function() {
    function getFullName (firstName,lastName) {
        return `${firstName} ${lastName}`;
    }

    let names = ["Charles","King","James","King","Maggie","King"];

    //spread array as args
    let fullName = getFullName(...names);

    console.log(fullName);

    //combine with destruturing
    let [ cFirst, cLast, ...rest ] = names;

    console.log([cFirst,cLast,rest]);

    //or array literal concatenation
    let who = ["Spongebob","Squarepants"]
    let where = [...who, "lives in", "a", "pineapple","under","the","sea"];

    console.log(where);

    //use for push and unshift
    let start = [1,2,3];
    let end = [4,5,6];
    let more = [7,8,9];

    end.unshift(...start);
    end.push(...more);
    //tada
    console.log(end);
};