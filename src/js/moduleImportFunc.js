//named default and others
import taskjs, { spawn, 
    enqueue, 
    truthy, 
    falsy, 
    read_only, 
    Task 
} from './moduleFunc';

//grab just the default export
import taskjs1 from './moduleFunc';
//just named
import { spawn1, enqueue1 } from './moduleFunc';

//import and rename
import { spawn2 as s, enqueue2 } from './moduleFunc';

//entire module as an object
import * as task from './moduleFunc';

//load the module, don’t import anything
import './moduleFunc';

export default function() {
	console.log('imported truthy =>', truthy);
}//