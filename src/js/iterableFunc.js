export default function() {
    //strings are iterable!
    const myName = "Charlie King";

    //so are arrays, as you would expect
    let myHeight = ['5','11'];

    //grab the chars
    for ( let char of myName ) {
      console.log(char);
    }

    //grab the items in array
    for ( let item of myHeight ) {
      console.log(item);
    }

    //spread needs iterables
    console.log(`Spreading: ${[...myName]}`);
    console.log(`Spreading: ${[...myHeight]}`);

    //so does destructing
    let [c,h,a] = [...myName];
    console.log(`Destructuring ${c} ${h} ${a}`);

    //you can also grab the iterator
    let stringIterator = myName[Symbol.iterator]();

    for ( let i=0; i < 3; i++ ) {
       console.log( stringIterator.next() );
    }
};