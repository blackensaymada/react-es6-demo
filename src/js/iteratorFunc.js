export default function() {
    //obligatory fibonacci
    let fibonacci = {
      // notice the special Symbol.iterator key
      [Symbol.iterator]() {
        let pre = 0, cur = 1;
        return {
          next() {
            [pre, cur] = [cur, pre + cur];
            return { done: false, value: cur }
          }
        }
      }
    }

    //Iterable can be iterated with for...of 
    //ensure to break for infinite iterables
    for (var n of fibonacci) {
      if (n > 1000) {
        break;
      }
      console.log(n);
    }
};