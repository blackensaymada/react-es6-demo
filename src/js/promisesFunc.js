
//simulate some async task
function someLongTask() {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            resolve("someLongTask");
        }, 1000);
    });
}
//simulate some other async task
function someOtherLongTask(msg) {
        return new Promise(function(resolve,reject){
            setTimeout(function(){
                 resolve("someOtherLongTask says: " +msg);
            },1500);
    });
}

export default function() {
    //run a task
    someLongTask()
    .then(function (msg) {
        console.log("Single: "+ msg);
    });

    //run chained tasks
    someLongTask()
    .then(someOtherLongTask)
    .then(function (msg) {
        console.log("Chained: "+msg);
    });

    //or wait for all of them
    var promises = [someLongTask(), someOtherLongTask("Hello")];

    Promise.all(promises).then(function(results) {
        var msg1 = results[0];
        var msg2 = results[1];
        console.log("All: " + msg1+ " then "+ msg2);
    });
};