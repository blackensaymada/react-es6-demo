//create a logger facade
export class Logger {
    //constructor
    constructor (type = "Info") {
        this.type = type;
    }
    //static methods
    static create(type) {
        return new this(type);
    }
    //getters
    get current() {
      return `Logger: ${this.type}`;
    }
    //and setters
    set current(type) {
        this.type = type;
    }
    log (message) {
        let msg = `%c ${new Date().toISOString()}: ${message}`;
    
        switch (this.type) {
            case "Info":
                console.log(msg,
                'background:#659cef;color:#fff;font-size:14px;'
                );
                break;
            case "Error":
                console.log(msg,
                'background: red; color: #fff;font-size:14px;'
                );
                break;
            case "Debug":
                console.log(msg,
                'background: #e67e22; color:#fff; font-size:14px;'
                );
                break;
            default:
                console.log(msg);
        }
    }
}