import { Logger } from './logger';

export default function() {
    //create an instance of our logger
    const debugLogger = new Logger("Debug");
    debugLogger.log("Hello");
    debugLogger.log(debugLogger.current);

    //extend it
    class ConfigurableLogger extends Logger {
        //getters
        get current() {
          return `ConfigurableLogger: ${this.type}`;
        }
        log (message, type) {
            this.type = type;
            super.log(message);
        }
    }

    //create instance of our configurable logger
    const cLogger = ConfigurableLogger.create("Debug");
    cLogger.log("Configurable Logger", "Info");
    cLogger.log(cLogger.current);

    cLogger.log(cLogger instanceof ConfigurableLogger); // true
    cLogger.log(cLogger instanceof Logger); // true
};